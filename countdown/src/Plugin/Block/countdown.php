<?php

namespace Drupal\countdown\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\node\Entity\Node;
/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "countdown_block",
 *   admin_label = @Translation("Countdown block"),
 *   category = @Translation("Countdown World"),
 * )
 */
class countdown extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {

       /* $node = Drupal::request()->attributes->get('node');
        #$node = menu_get_object(); //Drupal 7.x

        if (isset($node->nid)) {
            $nid = $node->nid;
        }
       */

        $node = \Drupal::routeMatch()->getParameter('node');
        if ($node instanceof \Drupal\node\NodeInterface) {
            $nid = $node->id();
        }
        $data = \Drupal\node\Entity\Node::load($nid);
        $field = $data->get('field_event_date')->getValue();
        $service = \Drupal::service('countdown.poslji_datum');
        $pogoj = $service->vrni_pogoj($field[0]['value']);
        if($pogoj > 0) {
            switch($pogoj) {
                case 1:
                    $izpis = $pogoj . " day left until event starts." /* . "XXXXXXX" . $pogoj*/;
                    break;
                default:
                    $izpis = $pogoj . " days left until event starts." /* . "XXXXXXX" . $pogoj*/;
                    break;
            }
        }
        else if($pogoj < 0) {
            $izpis = "This event already passed." /*. "XXXXXXX" . $pogoj*/;
        }
        else if($pogoj == 0) {
            $izpis = "This event is happening today." /*. "XXXXXXX" . $pogoj*/;
        }

        return array(
            '#markup' => $izpis /*. "     " . $field[0]['value']*/,
        );
    }

     public function getCacheMaxAge() {
        return 0;
    }

}

<?php

namespace Drupal\countdown\Services;

/**
 * Class CustomService.
 */
class Datum {

    /**
     * Constructs a new CustomService object.
     */
    public function __construct() {

    }

    public function vrni_pogoj($a) {
        $date = strtotime($a);
        $remaining = $date - time();
        $days_remaining = floor($remaining / 86400);
        if($days_remaining == -1 && date("l") == date("l", $date)) {
            return 0;
        }
        if(date("l") != date("l", $date) && $days_remaining == 0) {
            return 1;
        }
        return $days_remaining;
    }
}